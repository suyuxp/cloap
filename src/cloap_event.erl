-module(cloap_event).
-export([start_link/0,
         add_handler/2,
         delete_handler/2
        ]).
-export([login/1,
         action/4
        ]).
-define(SERVER, ?MODULE).

start_link() ->
    gen_event:start_link({local, ?SERVER}).

add_handler(Handler, Args) -> 
    gen_event:add_handler(?SERVER, Handler, Args).

delete_handler(Handler, Args) ->
    gen_event:delete_handler(?SERVER, Handler, Args).

login(Who) ->
    gen_event:notify(?SERVER, {login, Who}).

action(Who, App, Action, Data) ->
    gen_event:notify(?SERVER, {action, Who, App, Action, Data}).
