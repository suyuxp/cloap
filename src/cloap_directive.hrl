% --------------------------------------------------------------------------------
% Record to Proplist Macro
% --------------------------------------------------------------------------------
-define(R2P(Record), record_to_proplist(#Record{} = Rec) ->
           lists:zip(record_info(fields, Record), tl(tuple_to_list(Rec)))).

