# 前言
---

__[CLOAP](http://git.oschina.net/makertimes/cloap)__ 是企业开源云办公平台的简称，是 __[创客时代](http://makertimes.cn)__ 旗下的开源产品，目标是为了降低企业应用开发的复杂度，其中心思想来自于SOA，即面向服务架构。

平台是采用 __Erlang__ 开发的，但这并不影响模块开发的语言选择，开发者可以选择自己喜爱或者擅长的语言进行模块开发。

__[CLOAP](http://git.oschina.net/makertimes/cloap)__ 的使用是有一些限制的，所以请开发者在选用时认真考虑。这些限制包括：

* 平台的运行环境在 __Linux__ 下，很抱歉在 __Windows__ 下将无法得到我们的技术支援
* 前后端分离，前端使用 __AngularJS__ , 后端采用 __Restful__ 或者 __WebSocket__
* 只支持 __HTML5__ 标准的浏览器， __IE8__ 以下不受支持，未来也不会支持
* 平台还在不断发展中，目前可能无法满足一些企业的特定要求，请在不确定时尽量提前与我们进行邮件交流，如有需要，请发送咨询邮件到 __<cloap@makertimes.cn>__


__[CLOAP](http://git.oschina.net/makertimes/cloap)__ 的目标是为了简单易用，所以建议开发者尽量选取轻量级的开发框架，以下是我们在实际开发中采用过的框架，供大家参考。

1. Sinatra【Ruby】
2. Cowboy【Erlang】
3. Django【Python】

也欢迎开发者告知我们更多的选择，仍请发送邮件到 __<cloap@makertimes.cn>__ 。