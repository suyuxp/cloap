# coding: utf-8
require 'rubygems'
require 'msgpack/rpc'
require 'nokogiri'
require 'open-uri'

url = "http://news.cnpc.com.cn"
doc = Nokogiri::HTML(open(url))

news = doc.css('td a.z14012').map do |link|
  {subtitle: link.text, url: link["href"], update: Date.today.to_s}
end


rpc = MessagePack::RPC::Client.new("127.0.0.1", 9199)
rpc.timeout = 1000


spider_id = rpc.call(:register, {category: "行业新闻", title: "中石油新闻"})["ok"]["id"]
rpc.call(:refresh, spider_id, news)

p rpc.call(:list)


# ======== Password Cache ========
rpc.call(:cache_password, "test", "abcdefg")

