#!/usr/bin/env ruby
# coding: utf-8

require 'msgpack/rpc'

$port = 65501
port = $port

class MyServer
  def initialize(svr)
    @svr = svr
  end

  def refresh login, passwd
    p "msgpack call ......"
    [{category: "电子公文", subject: "待阅公文", items: [{subtitle: "关于发布新闻门户事宜", url: "http://127.0.0.1:8080/news/1", update_at: "2014-09-30 10:00:00"}]}]
  end

  def exception
    raise "raised"
  end

  def async
    as = MessagePack::RPC::AsyncResult.new
    @svr.start_timer(1, false) do
      as.result "async"
    end
    as
  end

  def async_exception
    as = MessagePack::RPC::AsyncResult.new
    @svr.start_timer(1, false) do
      as.error "async"
    end
    as
  end

  private
  def hidden
  end
end

def start_server port

  svr = MessagePack::RPC::Server.new
  svr.listen("0.0.0.0", port, MyServer.new(svr))
  Thread.start do
    svr.run
#    svr.close
  end

  return svr
end

svr = start_server port
gets
svr.stop

