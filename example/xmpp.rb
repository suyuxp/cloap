# -*- encoding : utf-8 -*-  
#该程序主要是向openfire发送信息  
#用户已经在openfire注册了，测试帐号是：tester001,密码是123456  
#接受信息帐号：tester002，密码: 123456  
#openfire帐号已经启动了  
require 'xmpp4r/client'  
include Jabber  
  
Jabber::debug = true # 开启jabber的debug模式  
  
#----------------------------用户登录---------------------------------  
server_str = 'robot@hackerlab.cn/testing' #用户名@服务器地址/资源号(资源号可以任意设定)  
jid = JID::new(server_str)  
password = 'ts3qdf'  
cl = Client::new(jid)  
cl.connect  
cl.auth(password)  
  
#----------------------------发送简单的消息-------------------------------  
to = "suyu@hackerlab.cn"  
subject = "测试xmpp4r,发送消息"  
body = "\n此处是消息的主题部分，应该可以看到很多字哦。"  
m = Message::new(to, body).set_type(:normal).set_id('1').set_subject(subject)  
cl.send m  
